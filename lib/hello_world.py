import parsekit
import time

class Say(parsekit.Step):

    message = parsekit.Argument(
            'Message to say.',
            default='Hello, world!',
            required=False,
            type=str)

    def run(self, input_, metadata):
        self.log.info(self.options.message)
        return input_, metadata

class Sleep(parsekit.Step):

    update_interval = parsekit.Argument(
            'Interval of seconds in which to update sleep status.',
            default=60,
            required=False,
            type=int)

    sleep = parsekit.Argument(
            'Number of seconds to sleep.',
            default=300,
            required=False,
            type=int)

    def run(self, input_, metadata):
        sleep = self.options.sleep

        interval = self.options.update_interval
        if interval <= 0 or interval > sleep:
            interval = sleep

        while sleep > 0:
            self.log.info("{} seconds of sleep remaining.".format(sleep))
            time.sleep(interval)
            sleep -= interval
            interval = min(interval, sleep)

        return input_, metadata
